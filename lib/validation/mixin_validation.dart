mixin CommonValidator {
  String? validateEmail(String? value) {
    if (!value!.contains('@')) {
      return 'Email must contain \'@\'';
    } else if (!value!.contains('.')) {
      return 'Email must contain \'.\'.';
    }
    return null;
  }

  String? validatePassword(String? value) {
    if (value!.length < 8) {
      return 'Password must be at least 8 characters.';
    } else if (!value.contains(RegExp(r'[A-Z]'))) {
      return 'Password must contain an uppercase character.';
    } else if (!value.contains(RegExp(r'[a-z]'))) {
      return 'Password must contain a lowercase character.';
    } else if (!value.contains(RegExp(r'[!@#$%^&*(),.?":{}|<>]'))) {
      return 'Password must contain a special character.';
    }
    return null;
  }
}
